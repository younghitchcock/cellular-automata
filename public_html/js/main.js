
/* global THREE */

//window.addEventListener( 'resize', onWindowResize, false );
$(window).resize(function(){
    onWindowResize();
});

/*Handles window resizes. */
function onWindowResize(){    
    
    w = $(document).width();
    h = $(document).height();
    
    /*For the ortho camera*/
    if (camera.hasOwnProperty("left")){
        camera.left = w / - orthoOffset;
        camera.right = w  /  orthoOffset;
        camera.top = h / orthoOffset;
        camera.bottom = h / - orthoOffset;
    }
    /*For the perspective camera*/
    else if (camera.hasOwnProperty("apsect")){        
        camera.aspect = w/h;
    }
    
    camera.updateProjectionMatrix();
    
    renderer.setSize( window.innerWidth, window.innerHeight );
}

/***DETECTOR****/
if (!Detector.webgl) {
    
    var body = $('body');
    body.text("Sorry, your browser doesn't support WebGL!");
    body.append("<br><a href=\"http://get.webgl.org\"> Click here to get webGL poppin'</a>");
    body.css("text-align", "center");
    body.css("margin-top", "10%");
    body.css("font-size", "20px");
    
    $("#three-canvas").remove();
    $("#controls").remove();
}

var w = $(window).width();
var h = $(window).height();


/**********************************Necessary THREE.JS setup things*************************************/
/*Create the renderer, set its size and add it to the page*/
var renderer = new THREE.WebGLRenderer();
renderer.setSize(w, h);
var canvas = $('#three-canvas');
canvas.append(renderer.domElement);

/*Create camera (orthographic means you DONT have FORESHORTENING)*/
//var camera = new THREE.OrthographicCamera( w / - 10, w / 10, h / 10, h / - 10, 1, 1000); //Frustrum lol
var fov = 250;
var camera = new THREE.PerspectiveCamera(fov, window.innerWidth / window.innerHeight, 0.1, 1000);
camera.position.z = 5;

/*Create scene/stage*/
var scene = new THREE.Scene();
scene.add(camera);

/*Create a light source*/
var light = new THREE.AmbientLight( 0x222222 );
scene.add(light);


/**********************WORKSPACE**************************/



var geometry = new THREE.BoxGeometry( 1, 1, 1 ); 
var material = new THREE.MeshLambertMaterial(); 
var cube = new THREE.Mesh( geometry, material ); 
//scene.add( cube ); 

var width = 50;
var height = 50;

var grid = buildGrid(width, height, 0.1,scene);

scene.add(grid);


$(window).click(function(){
    
    var dick = new Date().getTime();
//    for (var x = 0; x < width; x++){
//        for (var y = 0; y < height; y++){
//            grid.material.materials[getIdx(x,y,width, height)].color = new THREE.Color(Math.random(), Math.random(), Math.random());
//        }
//    }
    runOneGeneration(grid, width, height);
    console.log("Time to randomise colour: ", new Date().getTime() - dick);
});

function runOneGeneration(grid, width, height){
    
    var tempGrid = [];
    for (var x = 0; x < width; x++){
        for (var y = 0; y < height; y++){
            
            var curColor = grid.material.materials[getIdx(x,y,width, height)].color;
            var newColor = new THREE.Color(0,0,0);
            if (curColor.r < 0.5){
                newColor.b = 0.5;
            }
            newColor.r = curColor.r; //CLONE??
            tempGrid.push(newColor);
        }
    }
    
    for (var x = 0; x < width; x++){
        for (var y = 0; y < height; y++){          
            var idx = getIdx(x,y,width, height)
           grid.material.materials[idx].color = tempGrid[idx];
        }
    }
                

}



/************************END WORKSPACE************************/

function render() { 

    //    grid.rotation.y += 0.01;
    
    requestAnimationFrame( render ); 
    renderer.render( scene, camera ); 

} render();

/*1D -> 2D*/
function getIdx(x, y, w, h){
    var pos = (y * w) + x;
    if (pos > w * h){ //Bounds check
        return 0;
    } else {
        return pos;
    }    
}



/*** SHAPE CREATION ***/
function buildGrid(w, h, size, scene){

    var now = new Date().getTime();

    var gridGeo = new THREE.Geometry();
    
    var originalSquare = createRect(0, 0, size, size);
    var matPos = 0;
    var materials = [];        
        
    var xPos = -((w/2) * size);
    var yPos = -((h/2) * size);;
    for ( var y = 0; y < h; y++){
        for ( var x = 0; x < w; x++){
            
            var rando = Math.random();
            
            /*FASTEST WAY TO DO SHIT IS MERGING GEOMETRY APPARENTLY*/
            var plane = originalSquare.clone();
            plane.position.x = xPos;
            plane.position.y = yPos;
            //            plane.position.z = rando;
            
            plane.updateMatrix();            
            gridGeo.merge(plane.geometry, plane.matrix, matPos++);
                        
            materials.push( new THREE.MeshBasicMaterial( {color: new THREE.Color( rando, 0, 0 ) , side: THREE.DoubleSide} ) );
            
            xPos += size;   //Advance X            
        }
        
        yPos += size;           //Advance Y
        xPos = -((w/2) * size); //Reset X
    }
    
    var material = new THREE.MeshFaceMaterial(materials);
    var grid = new THREE.Mesh(gridGeo, material);
    
    
    console.log("Time to create grid: ", new Date().getTime() - now);
    return grid;
    
}

function createRect(x, y, w, h){
    var rectShape = new THREE.Shape();
    rectShape.moveTo( 0,0 );
    rectShape.lineTo( 0, w );
    rectShape.lineTo( h, w );
    rectShape.lineTo( h, 0 );
    rectShape.lineTo( 0, 0 );
    
    
    var rectGeom = new THREE.ShapeGeometry( rectShape );
    var rectMesh = new THREE.Mesh( rectGeom, new THREE.MeshBasicMaterial({color: new THREE.Color(0,0,0)}) ) ;
    
    rectMesh.position.x = x;
    rectMesh.position.y = y;
    
    return rectMesh;
}
